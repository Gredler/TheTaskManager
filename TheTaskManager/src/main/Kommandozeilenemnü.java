package main;
import java.util.Scanner;

public class Kommandozeilenemn� 
{

	Scanner scan;

	public Kommandozeilenemn�() 
	{
		this.scan = new Scanner(System.in);
	}

	public void Men�Anzeigen() 
	{
		System.out.println();
		System.out.println("MEN�");
		System.out.println("1) Alle Aufgaben Anzeigen");
		System.out.println("2) Aufgaben durchsuchen");
		System.out.println("3) Deadlines anzeigen");
		System.out.println("4) Neue Aufgabe Hinzuf�gen");
		System.out.println("5) Aufgabe erledigt");
		System.out.println("x) Beenden");
	}

	public void start() 
	{
		String s = "-";
		while (!s.equals("x")) 
		{
			Men�Anzeigen();
			s = scan.nextLine();
			switch(s)
			{
			case "1":
				Main.lib.alleAnzeigen();
				break;
			case "2":
				System.out.print("Gesuchter Text: ");
				Main.lib.aufgabenDurchsuchen(scan.nextLine());
				break;
			case "3":
				Main.lib.deadlinesAnzeigen();
				break;
			case "4":
				Main.lib.aufgabeHinzuf�gen();
				break;
			case "5":
				Main.lib.aufgabeErledigt();
			default:
				break;
			}
		}
		scan.close();
	}
}

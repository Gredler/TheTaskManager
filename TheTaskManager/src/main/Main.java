package main;

import java.util.GregorianCalendar;

public class Main 
{
	public static Aufgabensammlung lib;
	
	public static void main(String[] args) 
	{
		lib = new Aufgabensammlung();
		Aufgabe a = new Aufgabe("Programmieren", new GregorianCalendar());
		lib.add(a);
		a = new Aufgabe("Testen", new GregorianCalendar());
		lib.add(a);
		a = new AufgabeMitDeadline("Abgabe", new GregorianCalendar(), new GregorianCalendar(2017, 02, 01));
		lib.add(a);
		a = new AufgabeMitDeadline("Abgabe old", new GregorianCalendar(), new GregorianCalendar(2015, 00, 01));
		lib.add(a);
		
		Kommandozeilenemn� men� = new Kommandozeilenemn�();
		men�.start();
	}
}

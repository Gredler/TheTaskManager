package main;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Aufgabensammlung 
{
	private Scanner scan = new Scanner(System.in);
	private ArrayList<Aufgabe> aufgaben;
	
	public Aufgabensammlung() 
	{
		aufgaben = new ArrayList<>();
	}

	public ArrayList<Aufgabe> getAufgaben() 
	{
		return aufgaben;
	}

	public void setAufgaben(ArrayList<Aufgabe> aufgaben) 
	{
		this.aufgaben = aufgaben;
	}
	
	public void add(Aufgabe a) 
	{	
		aufgaben.add(a);
	}
	
	public void remove(Aufgabe a) 
	{
		aufgaben.remove(a);
	}
	
	public void remove(int index) 
	{
		aufgaben.remove(index);
	}
	
	public void alleAnzeigen() 
	{
		for(int i = 0; i < aufgaben.size(); i++)
		{
			System.out.println("[" + i + "] " + aufgaben.get(i).toString());
		}
	}
	
	public void aufgabenDurchsuchen(String text)
	{
		for(Aufgabe a : aufgaben)
		{
			if(a.getAufgabentext().equals(text)) System.out.println(a.toString());
		}
	}
	
	public void aufgabeHinzufügen() 
	{
		System.out.println("Was für eine Aufgabe wollen Sie hinzufügen?");
		System.out.println("1) Aufgabe ohne Deadline");
		System.out.println("2) Aufgabe mit Deadline");
		
		String s = scan.nextLine();
		switch(s)
		{
			case "1": System.out.print("Text: ");
				String text1 = scan.nextLine();
				Main.lib.add(new Aufgabe(text1, new GregorianCalendar()));
				break;
			case "2": System.out.print("Text: ");
				String text2 = scan.nextLine();
				System.out.println("Deadline:");
				System.out.print("	Year: ");
				int year = 2017;
				try
				{
					year = Integer.parseInt(scan.nextLine());
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				
				int month = -1;
				do
				{
					System.out.print("	Month: ");

					try
					{
						month = Integer.parseInt(scan.nextLine());
					}
					catch(Exception e)
					{
						e.printStackTrace();;
					}
				}
				while (month < 1 || month > 12);
				month--;

				
				int day = 1;
				do
				{
					System.out.print("	Day: ");

					try
					{
						day = Integer.parseInt(scan.nextLine());
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
				while (day < 1 || day > 31);

				
				GregorianCalendar currentDate = new GregorianCalendar();
				GregorianCalendar deadline = new GregorianCalendar(year, month, day);
				
				if(deadline.before(currentDate)) deadline = currentDate;
				
				
				Main.lib.add(new AufgabeMitDeadline(text2, currentDate, deadline));
			
			default:
				break;
		}
	}

	public void deadlinesAnzeigen() 
	{
		for(Aufgabe a : aufgaben)
		{
			if(a instanceof AufgabeMitDeadline)
			{
				GregorianCalendar currentDate = new GregorianCalendar();
				
				System.out.println(a.toString());
				
				if(((AufgabeMitDeadline) a).getDeadline().before(currentDate))
					System.out.println("Deadline überschritten!");
					
			}
		}
	}

	public void aufgabeErledigt() 
	{
		alleAnzeigen();
		
		int erledigt = aufgaben.size();
		try
		{
			erledigt = Integer.parseInt(scan.nextLine());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		if(erledigt != aufgaben.size()) aufgaben.remove(erledigt);
	}
}

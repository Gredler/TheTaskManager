package main;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class Aufgabe 
{
	private String aufgabentext;
	private GregorianCalendar daturmErstellung;
	
	public Aufgabe(String text, GregorianCalendar datum) 
	{
		aufgabentext = text;
		daturmErstellung = datum;
	}

	public String getAufgabentext() 
	{
		return aufgabentext;
	}

	public void setAufgabentext(String aufgabentext) 
	{
		this.aufgabentext = aufgabentext;
	}

	public GregorianCalendar getDaturmErstellung() 
	{
		return daturmErstellung;
	}
	
	public String toString() 
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd. MMMM yyyy");
		
		String ausgabe = "";
		ausgabe += super.toString();
		ausgabe += " Aufgabe: " + getAufgabentext() + "\nErstelldatum: " + dateFormat.format(getDaturmErstellung().getTime());
		return ausgabe;
	}
}

package main;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class AufgabeMitDeadline extends Aufgabe 
{
	private GregorianCalendar deadline;

	public AufgabeMitDeadline(String text, GregorianCalendar datum, GregorianCalendar deadline) 
	{
		super(text, datum);
		this.deadline = deadline;
	}

	public GregorianCalendar getDeadline() 
	{
		return deadline;
	}

	public void setDeadline(GregorianCalendar deadline) 
	{
		this.deadline = deadline;
	}
	
	@Override
	public String toString() 
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd. MMMM yyyy");
		
		String ausgabe = "";
		ausgabe += super.toString();
		ausgabe += "\nDeadline: " + dateFormat.format(getDeadline().getTime());
		return ausgabe;	
	}
}
